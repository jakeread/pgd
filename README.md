# Platonic Gantry Design

Fallen from the abstract spheres, these gantry designs come to us ~ d i s c o n n e c t e d ~ from any worldly reality, i.e any particular machine. This repository contains a parametric CAD model of the gantry that can be downloaded and configured for length / width / height, and integrated into a larger assembly - adding joinery / mounting patterns / etc.

![img-realworld](images/2019-11-18_pgd-1.jpg)

Linear motion is constrained with roller bearings, 625ZZ (5x16x5mm), on shoulder bolt shafts. Each rail is pinched with a small parallelogram flexure, for preload. No wiggling!

![img-carriagedetail](images/2019-11-18_pgd-2.jpg)

They use a NEMA17 stepper motor, with a 20T GT2 pulley and belt (6mm wide). They can be made from laser-cut acrylic (or delrin, or FR1/FR4 etc) and 3D printed parts. Joints between acrylic pieces are mediated with small 3D Printed inserts, to avoid stress concentrations that would normally cause acrlyic to crack.

![img-littlebits](images/2019-11-18_pgd-5.jpg)

The pulley idler is dead simple, and the belt is tensioned at the motor, which is mounted on slots.

| ![img-idler-and-motor-slots-twoup](images/2019-11-18_pgd-3.jpg) | ![img-idler-and-motor-slots-twoup](images/2019-11-18_pgd-4.jpg) |
| --- | --- |

## Parametric Fusion 360 CAD

![img-cad](images/2019-11-18_pgd-cad.png)

CAD (included in this repo) is available in this repo [under /cad](/cad) as a Fusion 360 `.f3z` file. The default setup is for a 590mm long gantry with 500mm of travel, that is about 90mm wide. The carriage is ~ 110mm long and 90mm wide.

The model is parametric, though, so these units can be changed. When you upload the `.f3z` into Fusion, you'll see 'PGD II' (the assembly, and beam) and the 'PGD Carriage' - in both of these files, you can use Fusion's `edit parameters` feature to modify the length, height, and widths of the gantry. `fair warning:` changing these parameters in large steps often causes issues in the CAD file. I.E, the length of the beam should only be modified in 10mm steps between increments - i.e. from 590 -> 580 -> 570 ... etc, to whichever length you would like. I see that this is painful: it is more a bug with Fusion's evaluation of some sketch elements. One must walk the gradient slowly.

## Static Rhino / STEP CAD

A Rhino model (used to draw this thing) is available in the repo [under /cad](/cad) as well: this may be useful for figuring out assembly order, etc. You can also slice and dice this model to suit, if Parametric CAD isn't your boat.

Finally, there is a `.step` solid model for import into any other CAD system, also [under /cad](/cad).

![img-cad](images/2019-11-19_pgd-rhino.png)

## BOM

Thing | Size | QTY | What For? | Vendor | PN / Link
--- | --- | --- | --- | --- | ---
**Material**
Acrylic | 1/4" 12x24" | 1 | Beam | McMaster | 8505K755
Tough PLA | ~ | ~ | Carriage, Idler, Joinery | Matter Hackers, etc | [link](https://www.matterhackers.com/store/l/light-blue-pro-series-tough-pla-filament-175mm-1kg/sk/M6E9T65K)
**Rollers and Idler**
625ZZ Bearings | 5x16x5mm | 11 | Rollers, Idler | VXB | [link](https://www.vxb.com/20-625ZZ-Shielded-5mm-Bore-Diameter-Miniature-p/625zz20.htm) or McMaster 6153K113
Shoulder Bolts | 5x6xM4 | 11 | Bearing Shaft | McMaster | 92981A146
Bearing Shims | 5x10x0.5mm | 22 | Bearing Standoffs | McMaster | 98089A331
Heat-Set Tapered Inserts | M4 | 11 | Shoulder Bolts -> 3DP Parts | McMaster | 94180A351
**Assembly of Carriage and Beam**
Heat-Set Tapered Insert | M3 | ~ 20 | Mechanical Design w/ Plastic | McMaster | 94180A331
Nylon Locknuts | M3 | ~ 30 | McMaster | 93625A100
Washers | M3 | use w/ all M3 SHCS | 200 | McMaster | 93475A210
M3 SHCS | 16mm Long | ~ 34 | Beam Joinery | McMaster | 91292A115
M3 SHCS | 10mm Long | 4 | Motor Mounting | McMaster | 91292A113
M3 FHCS | 16mm Long | 12 | Carriage Joinery | McMaster | 92125A134
**Belt and Pulley**
20T or 16T GT2 Pulley | 10mm or Wider, 5mm Bore | 4 | Power Transmission | Amazon or SDP/SI | [Amazon Link](https://www.amazon.com/BALITENSEN-Timing-Synchronous-Makerbot-Printer/dp/B07G81644C/)
10mm (or 9mm) Wide GT2 Belt | Find Steel-Core for Stiffness! | 5m | Power Transmission | Amazon or SDP/SI | [Amazon Link](https://www.amazon.com/Black-Timing-Printer-Machine-Meters/dp/B07PWL37RQ/)

To match the BOM to the object, I've this drawing with part callouts. Please pay special attention to the bearing shims!

The design features a handful of [Heat Set Inserts](https://hackaday.com/2019/02/28/threading-3d-printed-parts-how-to-use-heat-set-inserts/), which can be installed into 3D Printed parts with a soldering iron.

|![img-drawing](images/2019-11-19_pgd-dwg.png)|
| --- |
| **click to expand the drawing!** |

## Fabrication Notes

### 3D Printing

There are a handful of 3D Printed parts here: the entire carriage, the pulley idler, and the joinery between acrylic pieces. These are all designed to be printed without supports, and all have at least one large face to secure them to the bed. I print them in 'tough' PLA, which has a bit more give and is, well, tougher than normal PLA. That said, any filament will work: ABS, normal PLA, PETG, PC, etc. Your call.

### Lasers

On the CBA big-trotec, I used 100% power and 0.18% speed, with one pass, for this '1/4"' acrylic that is actually 5.7mm thick. This is - I think - an 80W machine.

To lay files out, I export a `.step` file from Fusion, pull that into Rhino, where it's easier to lay things out. In rhino, I use the `dupFaceBorder` command to get linework from the solids. I put interior cuts and exterior cuts on separate layers, color code those (the trotec cuts in this order: `black, red, blue, cyan, green`), and expor the DXF as `R12 Natural`.

DXFs can be had directly from Fusion as well: to do this, start a new sketch on the face that you would like to export. Close that sketch. Right-click on the sketch, and `export as dxf` - now you can import these one at a time into whatever laser-cutter-operating software you are using (illustrator, corel draw, etc) and 'print' them.
